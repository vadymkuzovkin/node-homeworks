const bodyParser = require('body-parser')
const express = require('express')
const morgan = require('morgan')

const fs = require('fs')
const path = require('path')

const app = express()
app.use(morgan('tiny'))

const validFileExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js']

// Finds file extension
const findFileExtension = (filename) => {
	const fileNameArr = filename.split('.')
	return fileNameArr[fileNameArr.length - 1]
}

app.use(express.json())

// Posts file
app.post('/api/files', (req, res) => {
	// Checks if "filename" parameter is entered
	if (req.body.filename === undefined) {
		return res.status(400).send({ message: "Please specify 'Filename' parameter" })
	}

	// Checks if correct extension is entered
	if (!validFileExtensions.includes(findFileExtension(req.body.filename))) {
		return res.status(400).send({
			message: 'Please enter apropriate file extension.' +
				' App supports next file extensions:  log, txt, json, yaml, xml, js'
		})
	}

	// Checks if "content" parameter is entered
	if (req.body.content.trim().length <= 0) {
		return res.status(400).send({ message: "Please specify 'content' parameter" })
	}

	// Writes file to "files" folder
	fs.writeFile(path.join(__dirname, 'files', req.body.filename),
		JSON.stringify({ content: req.body.content, password: req.body.password || '' }),
		err => {
			if (!err) {
				res.status(200).send({ message: "File created successfully" })
			}
		})
})

// Gets all files
app.get('/api/files', (req, res) => {
	// Reads files from "files" folder
	fs.readdir(path.join(__dirname, 'files'), (err, filesArr) => {

		if (err) {
			return res.status(400).send({ message: 'Client error' })
		}

		return res.status(200).send({
			message: 'Success',
			files: filesArr
		})

	})
})

// Gets specific file
app.get('/api/files/:filename', (req, res) => {

	const fileName = req.params.filename
	const filePath = path.join(__dirname, 'files', fileName)

	// Returns information about file 
	fs.stat(filePath, (err, stats) => {

		if (err) {
			return res.status(400).send({ message: `No file with '${fileName}' filename found` })
		}

		// Reads file 
		fs.readFile(filePath, (err, file) => {

			if (err) {
				return res.status(400).send(`No file with '${fileName}' filename found`)
			}

			const fileData = JSON.parse(file)

			// Checks if file has password and if it has, checks if correct password was entered
			if (fileData.password.trim()) {
				if (req.query.password !== fileData.password) {
					return res.status(400).send({ message: 'Please enter correct password!' })
				}
			}

			const fileMessage = {
				message: "Success",
				filename: fileName,
				content: fileData.content,
				extension: findFileExtension(fileName),
				uploadedDate: stats.birthtime
			}
			res.status(200).send(fileMessage)
		})
	})
})

//Deletes specific file
app.delete('/api/files/:filename', (req, res) => {
	const filePath = path.join(__dirname, 'files', req.params.filename)
	// Deletes file
	fs.unlink(filePath, err => {
		if (err) {
			return res.status(400).send({ message: 'File doesn`t exist!' })
		}

		res.status(200).send({ message: "File has been deleted successfully" })
	})
})

// Changes content of file your url should look like:
// If you have password: /api/files/test.txt?password=123&content={New content}
// If you do not have password: /api/files/test.txt?content={New content}

app.put('/api/files/:filename', (req, res) => {
	const filePath = path.join(__dirname, 'files', req.params.filename)
	let fileData;

	fs.readFile(filePath, (err, file) => {
		fileData = JSON.parse(file)
		if (req.query.content.trim() === '') {
			return res.status(400).send({ message: 'Please enter "content" parameter!' })
		}
		// Checks if password in query is correct
		if (fileData.password) {
			if (fileData.password !== req.query.password) {
				return res.status(400).send({ message: "Please enter correct password!" })
			}
		}

		const updatedFileData = { ...fileData }
		updatedFileData.content = req.query.content
		fs.writeFile(filePath, JSON.stringify(updatedFileData), err => {
			if (!err) {
				return res.status(200).send({ message: "File changed successfully!" })
			}
		})
	})
})

app.use((req, res, next) => {
	res.status(404).send({ message: "Page not found" });
})

app.use((error, req, res, next) => {
	res.status(500).send({ message: "Server error" });
});



app.listen(8080)